#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include "a2_helper.h"
#include <semaphore.h>
#include <pthread.h>
#include <fcntl.h>
#define debug 0

void dbgprintln(const char * to_be_printed){
    if(debug == 1){
        printf("%s\n",to_be_printed);
    }
}

sem_t proc7_thread1_can_start;
sem_t proc7_thread2_can_end;
sem_t proc8_mutex;
sem_t proc8_stop_enter;
sem_t proc8_stop_leave;

sem_t can_consume;
sem_t can_produce;
sem_t only_one;
sem_t only_one_1;

pthread_cond_t proc8_cond = PTHREAD_COND_INITIALIZER;
volatile int num_existing;
volatile int thread_13_exited;
volatile int total_threads = 47;
sem_t *proc7_t5_can_start;
sem_t *proc7_t5_has_ended;

void *th_fun5(void *arg){
    int th_num = *(int*)arg;
    
    if(th_num == 3){
        sem_wait(proc7_t5_has_ended);
    }
    info(BEGIN,5,th_num);

    info(END,5,th_num);
    if(th_num == 1){

        sem_post(proc7_t5_can_start);
    }
    return NULL;
}

void proc5BuildThreads(){
    
    proc7_t5_can_start = sem_open("my_sema_can_start",O_CREAT,0600,0);
    proc7_t5_has_ended = sem_open("my_sema_has_ended",O_CREAT,0600,0);

    pthread_t threads[7];
    int th_args[7];
    for(int i=1;i<7;i++){
        //dbgprintln("lolx");
        th_args[i] = i;
        pthread_create(&threads[i],NULL,th_fun5,&th_args[i]);
    }
    for(int i=1;i<7;i++){
        pthread_join(threads[i],NULL);
    }
}

void *th_fun7(void *arg){
    int th_num = *(int*)(arg);
    
    if(th_num == 1){
        sem_wait(&proc7_thread1_can_start);
    }
    if(th_num == 5){
        sem_wait(proc7_t5_can_start);
    }
    info(BEGIN,7,th_num);
    if(th_num == 2){
        sem_post(&proc7_thread1_can_start);
    }   
   
    if(th_num == 2){
        sem_wait(&proc7_thread2_can_end);
    }
   
    info(END,7,th_num);
     if(th_num == 1){
        sem_post(&proc7_thread2_can_end);
    }
    if(th_num == 5){
        sem_post(proc7_t5_has_ended);
    }
    return NULL;
}
void proc7BuildThreads(){
    proc7_t5_can_start = sem_open("my_sema_can_start",O_CREAT,0600,0);
    proc7_t5_has_ended = sem_open("my_sema_has_ended",O_CREAT,0600,0);

    pthread_t threads[6];
    int th_args[6];
    sem_init(&proc7_thread1_can_start,0,0);
    sem_init(&proc7_thread2_can_end,0,0);
    for(int i=1;i<6;i++){
        th_args[i] = i;
        pthread_create(&threads[i],NULL,th_fun7,&th_args[i]);
    }
    for(int i=1;i<6;i++){
        pthread_join(threads[i],NULL);
    }
}

void meeting_threads(){
    sem_wait(&only_one);
    //printf("meeting %d\n",total_threads);
        num_existing++;
        if(num_existing==6){
            info(END,8,13);
            total_threads--;
            sem_post(&proc8_stop_enter);
            sem_post(&can_produce);
        }
    sem_post(&only_one);
    sem_wait(&proc8_stop_enter);
    sem_post(&proc8_stop_enter);
}

void *th_fun8(void *arg){
    int th_num = *(int*)arg;
    sem_wait(&can_produce);
        info(BEGIN,8,th_num);
            if(total_threads==47){ 
          //      sem_post(&proc8_mutex);
                meeting_threads();
                
            }
        if(th_num == 13){
            return NULL;
        }   
        sem_wait(&proc8_mutex);
            total_threads--;
        sem_post(&proc8_mutex);
        info(END,8,th_num);
    sem_post(&can_produce);
    return NULL;
}

void proc8BuildThreads(){
    sem_init(&proc8_stop_enter,0,0);
    sem_init(&proc8_mutex,0,1);
    sem_init(&can_produce,0,6);
    sem_init(&only_one,0,1);

    pthread_t threads[48];
    int th_args[48];
    th_args[13] = 13;
    pthread_create(&threads[13],NULL,th_fun8,&th_args[13]);
    for(int i=1;i<6;i++){
        th_args[i] = i;
        pthread_create(&threads[i],NULL,th_fun8,&th_args[i]);
    }
    sem_wait(&proc8_stop_enter);
    sem_post(&proc8_stop_enter);
    for(int i=6;i<48;i++){
        if(i!=13){
            th_args[i] = i;
            pthread_create(&threads[i],NULL,th_fun8,&th_args[i]);
        }
    }

    for(int i=1;i<48;i++){
        pthread_join(threads[i],NULL);
    }
}



int main(){
    init();
    info(BEGIN, 1, 0);
    
    sem_unlink("my_sema_can_start");
    sem_unlink("my_sema_has_ended");

    pid_t pids[10];
    int statuses[10]={};
    for(int i=0;i<10;i++){
        pids[i] = -1;
    }
    //dbgprintln("Sunt 1 si am id %d",getpid())
    pids[2] = fork();
    if(pids[2] == -1){
        perror("Could not create process 2");
        return -1;
    }
    if(pids[2] == 0){
        info(BEGIN, 2, 0);
        //dbgprintln("Sunt 2 si m-am deschis din %d",getppid())
        pids[8] = fork();
        if(pids[8] == -1){
            perror("Could not create process 8");
            return -1;
        }
        if(pids[8] == 0){
            info(BEGIN, 8, 0);
           // dbgprintln("Sunt 8 si m-am deschis din 2");
            proc8BuildThreads();
            info(END, 8, 0); 
            exit(0);
        }
        waitpid(pids[8],&statuses[8],0);
        if(WIFEXITED(statuses[8]) == 1){
            info(END, 2, 0);
            exit(0);
        }
        else{
            dbgprintln("Some process didn't complete succesfully - 2");
        }
    }
    //mainu - nu e 2
    pids[3] = fork();
    if(pids[3] == -1){
        perror("Could not create process 3");
        return -1;
    } 
    if(pids[3] == 0){
        info(BEGIN, 3, 0);
        //dbgprintln("Sunt 3 si m-am deschis din %d",getppid());
        pids[5] = fork();
        if(pids[5] == -1){
            perror("Could not create process 5");
            return -1;
        }
        if(pids[5] == 0){
            info(BEGIN, 5, 0);     
            proc5BuildThreads();
            //dbgprintln("Sunt 5 si m-am deschis din 3");
            info(END, 5, 0);
            exit(0);
        }
        waitpid(pids[5],&statuses[5],0);
        if(WIFEXITED(statuses[5]) == 1){
            info(END, 3, 0);
            exit(0);
        }
        else{
            dbgprintln("Some process didn't complete succesfully - 3");
        }
    }
    //nu sunt 3- sunt mainu
    pids[4] = fork();
    if(pids[4] == -1){
        perror("Could not create process 4");
        return -1;
    }
    if(pids[4] == 0){
        info(BEGIN,4,0);

        info(END,4,0);
        exit(0);
    }
    //nu sunt 4 - sunt mainu
    pids[6] = fork();
    if(pids[6] == -1){
        perror("Could not create process 6");
        return -1;
    }
    if(pids[6] == 0){
        info(BEGIN,6,0);

        info(END,6,0);
        exit(0);
    }
    //nu sunt 6 - sunt mainu
    pids[7] = fork();
    if(pids[7] == -1){
        perror("Could not create process 7");
        return -1;
    }
    if(pids[7] == 0){
        info(BEGIN,7,0);
        proc7BuildThreads();
        info(END,7,0);
        exit(0);
    }
    //nu sunt 7, sunt mainu
    pids[9] = fork();
    if(pids[9] == -1){
        perror("Could not create process 9");
        return -1;
    }
    if(pids[9] == 0){
        info(BEGIN,9,0);

        info(END,9,0);
        exit(0);
    }
    //sunt mainu, la sfarsit
    int children_ids[]={2,3,4,6,7,9};
    int ok = 1;
    do{
        for(int i=0;i<6;i++){
            waitpid(pids[children_ids[i]],&statuses[children_ids[i]],0);
        }
        for(int i=0;i<6;i++){
            if(WIFEXITED(statuses[children_ids[i]]) == 0){
                dbgprintln("Something is wrong");
                ok = 0;
            }
        }
    }while(ok == 0);
    info(END,1,0);
    exit(0);
}
